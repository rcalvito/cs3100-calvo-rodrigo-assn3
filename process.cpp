#include "process.hpp"
#include <chrono>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>



extern bool quittingTime;

void handler2(int s){
}

void killFunc(){
	
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			kill(getpid(),0);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to send signal 0 with kill() 1000000 times\n";
		}
	}
	quittingTime=false;
	
	
}

void killFunc2(){
	signal(SIGUSR2,handler2);
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			kill(getpid(),SIGUSR2);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to send signal SIGUSR2 with kill() 1000000 times\n";
		}
	}
	quittingTime=false;
}
	
void nanoSleep(long amount){
	
	struct timespec tim;
	tim.tv_sec=0;
	tim.tv_nsec= amount;
	
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(i%250000==0 && i!=0){
				std::cout<<"Don't worry, it is working, it just takes a long time\n";
			}
			if(quittingTime){
				break;
			}
			nanosleep(&tim,&tim);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to nanosleep for "<<amount<<" nanosecod(s) 1000000 times\n";
		}
	}
	quittingTime=false;
}
	
	

void forkFunc(){
	std::cout<<"This process takes between 15 and 20 minutes to perform 1000000 times.\n";
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			if(i%50000==0 && i!=0){
				std::cout<<"Don't worry, it is working, it just takes a long time\n";
			}
			pid_t kid = fork();
			if(kid==0){
				exit(0);
			}
			else{
				wait(NULL);
			}
			
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " to fork() a 1000000 times\n";
		}
	}
	quittingTime=false;
}
	
	
	
	
	
	
	
	
