#include "filesystem.hpp"
#include <chrono>
#include <sys/stat.h>
#include <unistd.h>

extern bool quittingTime;


void getDir(){
	
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			get_current_dir_name();
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to get the current directory 1000000 times\n";
		}
	}
	quittingTime=false;
	
	
}

void changeDir(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			chdir(".");
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to change directory 1000000 times\n";
		}
	}
	quittingTime=false;
}


void accessFunc(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			access("/proc/self/exe",R_OK);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to access /proc/self/exe  1000000 times\n";
		}
	}
	quittingTime=false;
	
}


void syncFunc(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			sync();
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to sync()  1000000 times\n";
		}
	}
	quittingTime=false;
}
	
	
void changeMod(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			chmod("/proc/self/exe",S_IRWXU);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to change permissions 1000000 times\n";
		}
	}
	quittingTime=false;
}
	
	
	
void dup2Func(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			dup2(1,2);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to call dup2 1000000 times\n";
		}
	}
	quittingTime=false;
	
}
