#include "io.hpp"
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <chrono>

extern bool quittingTime;


void ioFunc(std::string location){
	const char* loc = location.c_str();
	
	
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			int place = open(loc,O_RDONLY);
			int destination =open("/dev/null",O_WRONLY);
			char* buffer[BUFSIZ];
	
			read(place,buffer,BUFSIZ);
			write(destination,buffer,BUFSIZ);
	
	
			close(place);
			close(destination);
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " read and write to a file 1000000 times\n";
		}
	}
	quittingTime=false;
	
	
	
	
	
	
	
	
}
