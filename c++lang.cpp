#include "c++lang.hpp"
#include <chrono>
#include <math.h>

extern bool quittingTime;


void arith(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			int dummy = 2+2;
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to perform 1000000 sums\n"; 
		}
	}
	quittingTime=false;

}

void mathLib(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			int dummy= sqrt(5);
			
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to perform 1000000 square roots\n"; 
		}
	}
	quittingTime=false;
}


void newDelChar(){
	
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			char* test = new char[1];
			delete test;
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to perform 1000000 new/delete operations\n"; 
		}
	}
	quittingTime=false;
}
	
	
void newChar(){
	while(!quittingTime){
		auto before = std::chrono::high_resolution_clock::now();
		for(int i=0;i<1000000;i++){
			if(quittingTime){
				break;
			}
			char* test = new char[1];
		}
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur = after - before;	
		if(!quittingTime){
			std::cout << "It took " << dur.count() << " seconds to perform 1000000 new operations\n"; 
		}
	}
	quittingTime=false;
}
	
	
