#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <string>
#include "c++lang.hpp"
#include "filesystem.hpp"
#include "process.hpp"
#include "io.hpp"

bool quittingTime=false;


void menu(){
	std::cout<<"Time wasting main menu PID:"<<getpid()<<std::endl;
	std::cout<<"	0) C++ arithmetic"<<std::endl;
	std::cout<<"	1) Math library()"<<std::endl;
	std::cout<<"	2) new/delete char[1]"<<std::endl;
	std::cout<<"	3) new char[1]"<<std::endl;
	std::cout<<"	4) kill(0)"<<std::endl;
	std::cout<<"	5) kill(SIGUSR2)"<<std::endl;
	std::cout<<"	6) nanosleep(1)"<<std::endl;
	std::cout<<"	7) nanosleep(1,000)"<<std::endl;
	std::cout<<"	8) nanosleep(1,000,000)"<<std::endl;
	std::cout<<"	9) fork()"<<std::endl;
	std::cout<<"	a) getcwd()"<<std::endl;
	std::cout<<"	b) chdir()"<<std::endl;
	std::cout<<"	c) access(/proc/self/exe)"<<std::endl;
	std::cout<<"	d) sync()"<<std::endl;
	std::cout<<"	e) chmod()"<<std::endl;
	std::cout<<"	f) dup2()"<<std::endl;
	std::cout<<"	g) I/O Bound loop over /dev/mmcblk0"<<std::endl;
	std::cout<<"	q) quit this fine program"<<std::endl;
}

void choice(std::string toDo="", std::string disc="/dev/mmcblk0"){
	
	if(toDo==""){
		std::cout<<">";
		std::cin>> toDo;
	}
	
	
	if(toDo=="0"){
		arith();
		menu();
		choice();
	}
	else if(toDo=="1"){
		mathLib();
		menu();
		choice();
	}
	else if(toDo=="2"){
		newDelChar();
		menu();
		choice();
	}
	else if(toDo=="3"){
		newChar();
	}
	else if(toDo=="4"){
		killFunc();
		menu();
		choice();
	}
	else if(toDo=="5"){
		killFunc2();
		menu();
		choice();
	}
	else if(toDo=="6"){
		nanoSleep(1);
		menu();
		choice();
	}
	else if(toDo=="7"){
		nanoSleep(1000);
		menu();
		choice();
	}
	else if(toDo=="8"){
		nanoSleep(1000000);
		menu();
		choice();
	}
	else if(toDo=="9"){
		forkFunc();
		menu();
		choice();
	}
	else if(toDo=="a"){
		getDir();
		menu();
		choice();
	}
	else if(toDo=="b"){
		changeDir();
		menu();
		choice();
	}
	else if(toDo=="c"){
		accessFunc();
		menu();
		choice();
	}
	else if(toDo=="d"){
		syncFunc();
		menu();
		choice();
	}
	else if(toDo=="e"){
		changeMod();
		menu();
		choice();
	}
	else if(toDo=="f"){
		dup2Func();
		menu();
		choice();
	}
	else if(toDo=="g"){
		ioFunc(disc);
		menu();
		choice();
	}
	else if(toDo=="q"){
		exit(0);
	}
	else{
		menu();
		choice();
	}
	
}


void handler(int s){
	quittingTime=true;
	
}


int main(int argc, char* argv[]){
	signal(SIGINT,handler);
	
	
	if(argc>2){
		std::string first = std::string(argv[1]);
		std::string second = std::string(argv[2]);
		if(first.size()>second.size()){
			choice(second,first);
		}	
		else{
			choice(first,second);
		}
	}
	else if(argc==2){
		std::string first = std::string(argv[1]);
		choice(first);
	}
	else{
		menu();
		choice();
	}
	



}
