
C++ arithmetic:  it is really quick to perform the operations and doesn't spend time in the kernel
Math library:    It takes longer to perform than just arithmetic but also quick and it doesn't spend time in the kernel
New/delete char: Takes longer than Math library with but still really quick, no time on the kernel
New char:
kill(0):         It isn't slow but not as fast as the firsts, takes about .6 seconds to perform a million and spends maybe like 75 % of the time in the kernel
kill(SIGRUS2):   Significantly slower than kill(0), spends maybe 80-90 % of time in the kernel.
nanosleep(1):    Really slow about a minute, doesn't use much of the cpu and spends some time on th kernel
nanosleep(1000): Slower than nanosleep(1) but really close, and doesn't use much of the cpu either with some kernel time
nanosleep(1000000):This one just takes forever it will take maybe between 10 and 20 minutes
fork():          This process is also really slow, it spends most of th etime in the kernel, and it uses multiple cores unlike the other processes that usually only use one
getcwd():        Takes about 6 seconds and spends maybe like 80% of time in the kernel.
chdir():         Takes about 1.5 seconds so relatively fast and spends almost all time in the kernel.
access():        Took about 7 seconds in average and spends all time in kernel
sync():          Takes about 30 seconds or so and spends all time in the kernel.
chmod():         It is relatively fast and it spends maybe like 95 % in the kernel, takes about 10 seconds to complete 1000000.
dup2():          Really fast, about .5 second every time, spends some time in the kernel maybe like 70-80 % changes cores and sometimes it doesn't use 100% of the core.
I/O bound loop:  it isn't thaat slow, takes about 10-11 seconds to perform a millon and spends maybe 80-90 % of the time in the kernel. 


I found this assigment really interesting, it really gives you an insight of how different programs work and what they actually do and what the kernel does for us, I used htop a lot for this and had some trouble with fork(), had to reeboot the rpi. It was really enjoyable to do this assignment.